import { render, screen } from '@testing-library/react';
import App from './App';
import '@testing-library/jest-dom';

test('renders app correctly', () => {
  render(<App />);
  const text = screen.getByText(/welcome/i);
  expect(text).toBeInTheDocument();
});
