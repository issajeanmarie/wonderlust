# WonderLust Project

## Installation process
> 1. yarn install (to install packages)
2. yarn start (to start the server)

## Used dependencies
1. redux
2. react-router-dom
3. formik
4. yup
5. antd
6. antd-icons